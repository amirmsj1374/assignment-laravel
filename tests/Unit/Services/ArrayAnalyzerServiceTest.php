<?php

namespace Tests\Unit\Service;

use App\Contracts\Analyzer;
use Tests\TestCase;

class ArrayAnalyzerServiceTest extends TestCase
{
    public function test_can_save_date_in_array()
    {
        $analyzer = app(Analyzer::class);
        $this->assertTrue($analyzer->send(["some-key"=> "some-value"]));
    }
}
