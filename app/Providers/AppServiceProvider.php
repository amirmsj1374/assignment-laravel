<?php

namespace App\Providers;

use App\Contracts\Analyzer;
use App\Observers\PostObserver;
use App\Post;
use App\Services\ArrayAnalyzerService;
use Exception;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Analyzer::class, function ($app) {
            if (config('analyzer.driver') === 'array') {
                return new ArrayAnalyzerService();
            }

            throw new Exception('The exchange rates driver is invalid.');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Post::observe(PostObserver::class);
    }
}
