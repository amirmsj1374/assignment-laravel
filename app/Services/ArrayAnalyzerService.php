<?php

namespace App\Services;

use App\Contracts\Analyzer;

class ArrayAnalyzerService implements Analyzer
{
    protected $analyzer;

    public function send(array $data): bool
    {
        try {
            $this->analyzer = $data;
            return true;

        } catch (\Throwable $th) {
            return false;
        }
    }
}
